package edu.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import edu.configuration.ObjectMapperProvider;
import edu.errors.DeserializerError;
import edu.errors.ErrorContainer;
import edu.model.General;
import edu.model.Offer;
import edu.service.impl.ValidationService;
import edu.util.ResponseController;
import io.swagger.annotations.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/general")
public class GeneralController {
    @Autowired
    @Qualifier(value = "objectMapperProvider")
    private ObjectMapperProvider objectMapperProvider;

    @Autowired
    private ValidationService validationService;

    @PostMapping(value = "/add", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(
                    name = "generalString",
                    dataType = "General"
            )}
    )
    public General add(@RequestBody String generalString) {
        General g = new General();
        try {
            g = objectMapperProvider.provide()
                    .readValue(generalString, new TypeReference<General>() {
                    });
            return g;
        } catch (Exception e) {
            return g;
        }
    }

    @PostMapping(value = "/create", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    @ApiImplicitParams({
            @ApiImplicitParam(
                    name = "generalString",
                    dataType = "General"
            )}
    )
    @ApiResponses(value = {
            @ApiResponse(
                    code = 200,
                    message = "Full response without errors",
                    response = General.class

            ),
            @ApiResponse(
                    code = 201,
                    message = "Short response without errors",
                    response = ShortResponse.class

            ),
            @ApiResponse(
                    code = 400,
                    message = "Full/Short response with all errors",
                    response = General.class
            ),
            @ApiResponse(
                    code = 207,
                    message = "Full/Short response with some errors\n" +
                            "If errors is empty - short response,\n" +
                            "Else - full response",
                    response = General.class

            )
    })
    public ResponseEntity<List<ErrorContainer>> create(@RequestBody String generalString, @RequestParam boolean isShort) throws IOException {
        List<ErrorContainer> response = new ArrayList<>();
        try {
            List<General> generals = objectMapperProvider.provide()
                    .readValue(generalString, new TypeReference<List<General>>() {
                    });
            for (General g : generals) {
                validationService.runValidation(g);
            }
            int errors = ResponseController.isErrors(generals);
            if (isShort && errors == 0) {
                response.addAll(ResponseController.getShortResponse(generals));
                return new ResponseEntity<>(response, HttpStatus.valueOf(201));
            }
            if (isShort && errors > 0 && errors < generals.size()) {
                response.addAll(ResponseController.getShortResponseWithErrors(generals));
                return new ResponseEntity<>(response, HttpStatus.valueOf(207));
            }
            if (isShort && errors == generals.size()) {
                response.addAll(ResponseController.getShortResponseWithErrors(generals));
                return new ResponseEntity<>(response, HttpStatus.valueOf(400));
            }
            response.addAll(generals);
            if(errors == generals.size()){
                return new ResponseEntity<>(response, HttpStatus.valueOf(400));
            }
            return errors!=0 ? new ResponseEntity<>(response, HttpStatus.valueOf(207)) : new ResponseEntity<>(response, HttpStatus.valueOf(200));
        } catch (Exception e) {
            throw e;
        }
    }

    @ApiModel
    public static class ShortResponse implements ErrorContainer {
        @ApiModelProperty(position = 0)
        private int sourceId;
        @ApiModelProperty(position = 1)
        private OffsetDateTime sourceTimestamp;
        @ApiModelProperty(position = 2)
        private List<Offer> offers;
        @ApiModelProperty(position = 3)
        private List<DeserializerError> errors;

        public ShortResponse() {
        }

        public int getSourceId() {
            return sourceId;
        }

        public void setSourceId(int sourceId) {
            this.sourceId = sourceId;
        }

        public OffsetDateTime getSourceTimestamp() {
            return sourceTimestamp;
        }

        public void setSourceTimestamp(OffsetDateTime sourceTimestamp) {
            this.sourceTimestamp = sourceTimestamp;
        }

        public List<Offer> getOffers() {
            return offers;
        }

        public void setOffers(List<Offer> offers) {
            this.offers = offers;
        }

        public void addError(DeserializerError error) {
            if (errors == null) {
                errors = new ArrayList<>();
            }
            errors.add(error);
        }

        public List<DeserializerError> getErrors() {
            return errors;
        }
    }
}
