package edu.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ObjectMapperProvider {
    private ObjectMapper objectMapper;

    public ObjectMapperProvider(ObjectMapper objectMapper) {
        this.objectMapper = objectMapper;
    }

    public ObjectMapper provide() {
        return this.objectMapper;
    }

}
