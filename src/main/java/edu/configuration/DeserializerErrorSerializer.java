package edu.configuration;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import edu.errors.DeserializerError;

import java.io.IOException;

public class DeserializerErrorSerializer extends StdSerializer<DeserializerError> {

    protected DeserializerErrorSerializer(Class<DeserializerError> t) {
        super(t);
    }

    public DeserializerErrorSerializer() {
        this(null);
    }

    @Override
    public void serialize(DeserializerError value, JsonGenerator gen, SerializerProvider provider) throws IOException {
        gen.writeStartObject();
        gen.writeStringField("path", value.getPath());
        gen.writeNumberField("sourceId", value.getSourceId());
        gen.writeStringField("code", value.getCode());
        gen.writeStringField("message", value.getMessage());
        gen.writeEndObject();
    }
}
