package edu.configuration;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonStreamContext;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.datatype.jsr310.deser.InstantDeserializer;
import edu.errors.DeserializerError;
import edu.errors.ErrorContainer;
import edu.errors.Fallout;
import edu.model.General;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

public class OffsetDateTimeDeserializer extends JsonDeserializer<OffsetDateTime> {

    @Override
    public OffsetDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        InstantDeserializer<OffsetDateTime> offsetDateTimeInstantDeserializer = InstantDeserializer.OFFSET_DATE_TIME;
        try {
            return offsetDateTimeInstantDeserializer.deserialize(jsonParser, deserializationContext);
        } catch (JsonProcessingException exc) {
            StringBuilder path = new StringBuilder();
            int sourceId;
            List<String> errorAttrs = new ArrayList<>();
            List<String> errorPath = new ArrayList<>();
            JsonStreamContext errorsContainerContext = jsonParser.getParsingContext();
            JsonStreamContext erroneousDto = errorsContainerContext.getParent();
            if (erroneousDto.getParent() != null) {
                errorAttrs.add(erroneousDto.getParent().getCurrentName() + "[" + erroneousDto.getCurrentIndex() + "]");
                errorAttrs.add(errorsContainerContext.getCurrentName());
                errorPath.add(errorsContainerContext.getCurrentName());
                while (errorsContainerContext != null && !(errorsContainerContext.getCurrentValue() instanceof ErrorContainer)) {
                    if (erroneousDto.getParent().getCurrentName() != null) {
                        errorPath.add(erroneousDto.getParent().getCurrentName() + "[" + erroneousDto.getCurrentIndex() + "]");
                    }
                    erroneousDto = errorsContainerContext.getParent().getParent();
                    errorsContainerContext = errorsContainerContext.getParent();
                }
                General g = (General) errorsContainerContext.getCurrentValue();
                sourceId = g.getSourceId();
                path.append('$');
                for (int i = errorPath.size(); i > 0; i--) {
                    path.append('.');
                    path.append(errorPath.get(i - 1));
                }
            } else {
                General g = (General) errorsContainerContext.getCurrentValue();
                sourceId = g.getSourceId();
                errorAttrs.add("root");
                path.append("$.");
            }
            ErrorContainer parent = (ErrorContainer) errorsContainerContext.getCurrentValue();
            DeserializerError errorMessage = new DeserializerError(Fallout.INCORRECT_DATE_TIME_FORMAT_ERROR_MESSAGE, errorAttrs.toArray(new String[0]), path.toString(), sourceId);
            parent.addError(errorMessage);
            return null;

        }
    }
}
