package edu.configuration;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;
import edu.errors.DeserializerError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;

import java.time.OffsetDateTime;

@Configuration("Config")
public class Config {
    @Autowired
    private Jackson2ObjectMapperBuilder builder;

    @Bean
    public ObjectMapperProvider objectMapperProvider() {
        ObjectMapper objectMapper = builder.createXmlMapper(false).build();
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        SimpleModule module =
                new SimpleModule("OffsetDateTimeDeserializerModule",
                        new Version(1, 0, 0, null, null, null));
        module.addSerializer(DeserializerError.class, new DeserializerErrorSerializer());
        module.addDeserializer(OffsetDateTime.class, new OffsetDateTimeDeserializer());
        objectMapper.registerModule(module);
        return new ObjectMapperProvider(objectMapper);
    }
}
