package edu.model;

import io.swagger.annotations.ApiModelProperty;

import java.time.OffsetDateTime;

public class Price {
    @ApiModelProperty(position = 0)
    private int priceId;
    @ApiModelProperty(position =1)
    private OffsetDateTime endDate;

    public Price() {
    }

    public Price(int priceId, OffsetDateTime endDate) {
        this.priceId = priceId;
        this.endDate = endDate;
    }

    public int getPriceId() {
        return priceId;
    }

    public void setPriceId(int priceId) {
        this.priceId = priceId;
    }

    public OffsetDateTime getEndDate() {
        return endDate;
    }

    public void setEndDate(OffsetDateTime endDate) {
        this.endDate = endDate;
    }
}
