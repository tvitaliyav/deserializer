package edu.model;

import edu.errors.DeserializerError;
import edu.errors.ErrorContainer;
import io.swagger.annotations.ApiModelProperty;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;

public class General implements ErrorContainer{
    @ApiModelProperty(position = 0)
    private int sourceId;
    @ApiModelProperty(position = 1)
    private OffsetDateTime sourceTimestamp;
    @ApiModelProperty(position = 2)
    private List<Offer> offers;
    @ApiModelProperty(position = 6)
    private List<DeserializerError> errors;

    @ApiModelProperty(position = 3)
    private Integer a;
    @ApiModelProperty(position = 4)
    private String b;
    @ApiModelProperty(position = 5)
    private Double c;

    public General(int sourceId, OffsetDateTime sourceTimestamp, List<Offer> offers) {
        this.sourceId = sourceId;
        this.sourceTimestamp = sourceTimestamp;
        this.offers = offers;
    }

    public General() {

    }

    public int getSourceId() {
        return sourceId;
    }

    public void setSourceId(int sourceId) {
        this.sourceId = sourceId;
    }

    public OffsetDateTime getSourceTimestamp() {
        return sourceTimestamp;
    }

    public void setSourceTimestamp(OffsetDateTime sourceTimestamp) {
        this.sourceTimestamp = sourceTimestamp;
    }

    public List<Offer> getOffers() {
        return offers;
    }

    public void setOffers(List<Offer> offers) {
        this.offers = offers;
    }

    public Integer getA() {
        return a;
    }

    public void setA(Integer a) {
        this.a = a;
    }

    public String getB() {
        return b;
    }

    public void setB(String b) {
        this.b = b;
    }

    public Double getC() {
        return c;
    }

    public void setC(Double c) {
        this.c = c;
    }

    public void addError(DeserializerError error) {
        if (errors == null) {
            errors = new ArrayList<>();
        }
        errors.add(error);
    }

    public List<DeserializerError> getErrors() {
        return errors;
    }

//    @Override
//    public String toString() {
//        return "General{" +
//                "sourceId=" + sourceId +
//                ", sourceTimestamp=" + sourceTimestamp +
//                ", offers=" + offers +
//                ", errors=" + this.getErrors().toString() +
//                '}';
//    }
}
