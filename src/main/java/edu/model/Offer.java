package edu.model;

import io.swagger.annotations.ApiModelProperty;

import java.time.OffsetDateTime;
import java.util.List;

public class Offer {

    @ApiModelProperty(position = 0)
    private int offerId;

    @ApiModelProperty(position = 1)
    private OffsetDateTime startDate;

    @ApiModelProperty(position = 2)
    private List<Price> prices;

    public Offer() {
    }

    public Offer(int offerId, OffsetDateTime startDate, List<Price> prices) {
        this.offerId = offerId;
        this.startDate = startDate;
        this.prices = prices;
    }

    public int getOfferId() {
        return offerId;
    }

    public void setOfferId(int offerId) {
        this.offerId = offerId;
    }

    public OffsetDateTime getStartDate() {
        return startDate;
    }

    public void setStartDate(OffsetDateTime startDate) {
        this.startDate = startDate;
    }

    public List<Price> getPrices() {
        return prices;
    }

    public void setPrices(List<Price> prices) {
        this.prices = prices;
    }
}
