package edu.errors;


import java.util.ArrayList;
import java.util.List;

public interface ErrorContainer {

    void addError(DeserializerError error);

    List<DeserializerError> getErrors();
}
