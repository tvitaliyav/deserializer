package edu.errors;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import edu.configuration.DeserializerErrorSerializer;
import org.apache.commons.lang3.ArrayUtils;

@JsonSerialize(using = DeserializerErrorSerializer.class)
public class DeserializerError {

    private String path;

    private int sourceId;

    private String code;

    private String message;

    public DeserializerError(Fallout fallout, String[] messageValues, String path, int sourceId) {
        this.code = fallout.getCode();
        this.message = ArrayUtils.isEmpty(messageValues) ?
                (fallout.getDetails()) :
                String.format(fallout.getDetails(), messageValues);
        this.path = path;
        this.sourceId = sourceId;
    }

    @Override
    public String toString() {
        return "DeserializerError{" +
                "path='" + path + '\'' +
                ", sourceId=" + sourceId +
                ", code='" + code + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public int getSourceId() {
        return sourceId;
    }

    public void setSourceId(int sourceId) {
        this.sourceId = sourceId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
