package edu.errors;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice(basePackages = "edu")
@Order(Ordered.HIGHEST_PRECEDENCE)
public class DeserializationExceptionHandler {
    @ExceptionHandler({RuntimeException.class})
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    DeserializerError handleRuntimeException(RuntimeException exception) {
        return new DeserializerError(Fallout.INCORRECT_DATE_TIME_FORMAT_ERROR_MESSAGE, null, null, 0);
    }

}
