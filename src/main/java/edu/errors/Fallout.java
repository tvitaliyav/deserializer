package edu.errors;

public enum Fallout {

    /**
     * <b>Details</b> require parameter: error object name and id
     */
    INCORRECT_DATE_TIME_FORMAT_ERROR_MESSAGE("DES_0000", "Wrong date time format", "Date format for %s is wrong"),

    /**
     * <b>Details</b> require parameter: error object name and id
     */
    INCORRECT_DATE_TIME_ATTRIBUTE_ERROR_MESSAGE("DES_0001", "Wrong date time range", "Date for %s is wrong"),

    /**
     * <b>Details</b> require parameter: error object name and id
     */
    ID_VALUE_SHOULD_BE_POSITIVE_ERROR_MESSAGE("DES_0002", "Wrong id", "ID value for %s should be below than zero"),


    UNEXPECTED_EXCEPTION_ERROR_MESSAGE("DES_0003", "Unexpected exception", "Unexpected exception"),

    ;

    private String code;
    private String title;
    private String details;

    Fallout(String code, String title, String details) {
        this.code = code;
        this.title = title;
        this.details = details;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }
}
