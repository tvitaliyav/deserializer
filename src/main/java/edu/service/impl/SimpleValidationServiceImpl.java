package edu.service.impl;

import edu.service.api.SimpleValidationService;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;

@Service
public class SimpleValidationServiceImpl implements SimpleValidationService {

    @Override
    public boolean validateDate(OffsetDateTime offsetDateTime) {
        return offsetDateTime != null && !offsetDateTime.isBefore(OffsetDateTime.parse("1970-01-01T00:00:00.000Z")) && !offsetDateTime.isAfter(OffsetDateTime.now());
    }

    @Override
    public boolean validateId(int id) {
        return id > 0;
    }
}
