package edu.service.impl;

import edu.errors.DeserializerError;
import edu.errors.Fallout;
import edu.model.General;
import edu.service.api.SimpleValidationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ValidationService {

    @Autowired
    private SimpleValidationService simpleValidationService;

    public void runValidation(General general) {
        validateGeneral(general);
        validateOffer(general);
        validatePrice(general);
    }

    private void validateGeneral(General general) {
        if (!simpleValidationService.validateId(general.getSourceId())) {
            general.addError(new DeserializerError(Fallout.ID_VALUE_SHOULD_BE_POSITIVE_ERROR_MESSAGE, new String[]{"root"}, "$.", general.getSourceId()));
        }
        if (!simpleValidationService.validateDate(general.getSourceTimestamp())) {
            general.addError(new DeserializerError(Fallout.INCORRECT_DATE_TIME_ATTRIBUTE_ERROR_MESSAGE, new String[]{"root"}, "$.", general.getSourceId()));
        }
    }

    private void validateOffer(General general) {
        for (int i = 0; i < general.getOffers().size(); i++) {
            if (!simpleValidationService.validateId(general.getOffers().get(i).getOfferId())) {
                String path = "$.offers[" + i + "].offerId";
                general.addError(new DeserializerError(Fallout.ID_VALUE_SHOULD_BE_POSITIVE_ERROR_MESSAGE, new String[]{path}, path, general.getSourceId()));
            }
            if (!simpleValidationService.validateDate(general.getOffers().get(i).getStartDate())) {
                String path = "$.offers[" + i + "].startDate";
                general.addError(new DeserializerError(Fallout.INCORRECT_DATE_TIME_ATTRIBUTE_ERROR_MESSAGE, new String[]{path}, path, general.getSourceId()));
            }
        }
    }

    private void validatePrice(General general) {
        for (int i = 0; i < general.getOffers().size(); i++) {
            for (int j = 0; j < general.getOffers().get(i).getPrices().size(); j++) {
                if (!simpleValidationService.validateId(general.getOffers().get(i).getPrices().get(j).getPriceId())) {
                    String path = "$.offers[" + i + "].prices[" + j + "].priceId";
                    general.addError(new DeserializerError(Fallout.ID_VALUE_SHOULD_BE_POSITIVE_ERROR_MESSAGE, new String[]{path}, path, general.getSourceId()));
                }
                if (!simpleValidationService.validateDate(general.getOffers().get(i).getPrices().get(j).getEndDate())) {
                    String path = "$.offers[" + i + "].prices[" + j + "].endDate";
                    general.addError(new DeserializerError(Fallout.INCORRECT_DATE_TIME_ATTRIBUTE_ERROR_MESSAGE, new String[]{path}, path, general.getSourceId()));
                }
            }
        }
    }
}

