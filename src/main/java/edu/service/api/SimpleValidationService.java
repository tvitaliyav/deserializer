package edu.service.api;

import java.time.OffsetDateTime;

public interface SimpleValidationService {

    boolean validateDate(OffsetDateTime offsetDateTime);

    boolean validateId(int id);
}
