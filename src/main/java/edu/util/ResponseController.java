package edu.util;

import edu.controllers.GeneralController;
import edu.errors.ErrorContainer;
import edu.model.General;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ResponseController {

    public static List<GeneralController.ShortResponse> getShortResponse(List<General> full) {
        List<GeneralController.ShortResponse> shortResponses = new ArrayList<>();
        for (General f : full) {
            GeneralController.ShortResponse shortGeneral = new GeneralController.ShortResponse();
            shortGeneral.setSourceId(f.getSourceId());
            shortGeneral.setOffers(f.getOffers());
            shortGeneral.setSourceTimestamp(f.getSourceTimestamp());
            shortResponses.add(shortGeneral);
        }
        return shortResponses;
    }

    public static int isErrors(List<General> obj) {
        int err = 0;
        for (General er : obj) {
            if (er.getErrors() != null && er.getErrors().size() > 0) {
                err++;
            }
        }
        return err;
    }

    public static Collection<? extends ErrorContainer> getShortResponseWithErrors(List<General> generals) {
        List<ErrorContainer> shortResp = new ArrayList<>();
        for (General g : generals) {
            if (g.getErrors() == null || g.getErrors().size() == 0) {
                shortResp.add(getShortResponse(g));
                continue;
            }
            shortResp.add(g);
        }
        return shortResp;
    }

    private static GeneralController.ShortResponse getShortResponse(General general) {
        GeneralController.ShortResponse shortGeneral = new GeneralController.ShortResponse();
        shortGeneral.setSourceId(general.getSourceId());
        shortGeneral.setOffers(general.getOffers());
        shortGeneral.setSourceTimestamp(general.getSourceTimestamp());
        return shortGeneral;
    }
}
